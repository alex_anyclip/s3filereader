package contracts;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;

public abstract class Reader implements IFileIterator {
    protected AmazonS3 s3;
    protected String bucket;
    protected String file;
    protected BufferedReader reader;
    private String nextLine;

    public Reader(){}

    public Reader(AmazonS3 s3connection, String bucket, String filePath) throws IOException {
        this.bucket = bucket;
        this.file = filePath;
        s3 = s3connection;
        getStream(this.file);
    }

    public abstract void getStream(String filePath) throws IOException;

    @Override
    public boolean hasNext() throws IOException {
        nextLine = reader.readLine();
        return nextLine != null;
    }

    @Override
    public String next() {
        return nextLine;
    }

    @Override
    public String nextLineString() {
        return nextLine;
    }

}
