package contracts;

import java.io.IOException;

public interface IFileIterator {
    public boolean hasNext() throws IOException;

    public String next();

    public String nextLineString();

}
