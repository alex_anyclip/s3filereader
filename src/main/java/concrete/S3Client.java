package concrete;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

public class S3Client {

    private AmazonS3 s3;

    public S3Client() {
        this(Regions.US_EAST_1);
    }

    public S3Client(Regions region) {
       this.s3 = AmazonS3ClientBuilder.standard().withRegion(region).build();
    }

    public AmazonS3 getS3client() {
        return s3;
    }
}
