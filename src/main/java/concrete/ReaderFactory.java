package concrete;

import contracts.Reader;

import java.io.IOException;

public class ReaderFactory {

    public static Reader getGzipReader(S3Client s3Client, String bucket, String path) throws IOException {
        return new GzipReader(s3Client, bucket, path);
    }
}
