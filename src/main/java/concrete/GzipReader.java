package concrete;

import com.amazonaws.services.s3.model.S3Object;
import contracts.Reader;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

public class GzipReader extends Reader {

    public GzipReader() {
    }

    public GzipReader(S3Client s3Client, String bucket, String filePath) throws IOException {
        super(s3Client.getS3client(), bucket, filePath);
    }

    @Override
    public void getStream(String filePath) throws IOException {
        S3Object object = s3.getObject(bucket, filePath);
        BufferedInputStream stream = new BufferedInputStream(new GZIPInputStream(object.getObjectContent()));
        super.reader = new BufferedReader(new InputStreamReader(stream));
    }
}
