import concrete.ReaderFactory;
import concrete.S3Client;
import contracts.Reader;

import java.io.IOException;

public class Example {
    public static void main(String[] args) throws IOException {
        String bucket = "anyclip-fictivious";
        String path = "qa/2017/10/17/13/ls.s3.nginx-qa-server-10.0.109.15.2017-10-17T13.08.txt.gz";
        S3Client s3Client = new S3Client();
        Reader reader = ReaderFactory.getGzipReader(s3Client, bucket, path);
        while (reader.hasNext()){
            System.out.println(reader.next());
        }
    }
}
